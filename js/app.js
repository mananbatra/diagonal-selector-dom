const mainContainer = document.getElementById("main-container");

const size=10;
let length=`calc(100%/${size})`;

for (let row = 1; row <= size; row++) {
  for (let col = 1; col <= size; col++) {
    const card = document.createElement("div");
    card.classList.add("card");
    card.id = `${row},${col}`;

    card.dataset.row=row;
    card.dataset.col=col;

    card.style.width=length;
    card.style.height=length;
    

    if (row%2==0) 
    {
      if (col % 2 === 0) {
        card.classList.add("white");
        
      } else {
        card.classList.add("black");
      }
    } else 
    {
      if (col % 2 === 0) {
        card.classList.add("black");
      } else {
        card.classList.add("white");
      }
    }

    mainContainer.appendChild(card);
  }
}


mainContainer.addEventListener("click", getTarget);

function getTarget({ target }) 
{
    
  let cards=document.querySelectorAll('.card');

  // console.log(cards);

  cards.forEach((card) =>
  {
      card.classList.remove("red");
  })

  const targetId=target;

  let row=+(targetId.dataset.row);
  let column=+(targetId.dataset.col);

   diagonalRightBottom(row,column);
   diagonalLeftDown(row,column);
   diagonalLeftUp(row,column);
   diagonalRightUp(row,column);

  //  let cardRed=document.querySelector(`[dataset-row="${row}"][dataset-col="${column}"]`);

    targetId.classList.toggle("red");
}


function diagonalRightBottom(row,column)
{

  while(row<=size && column<=size)
  {
    let cardRed=document.querySelector(`[data-row="${row}"][data-col="${column}"]`);

    cardRed.classList.toggle("red");

    ++row;
    ++column;
  }
}

function diagonalLeftUp(row,column)
{

  while(row>=1 && column>=1)
  {
    let cardRed=document.querySelector(`[data-row="${row}"][data-col="${column}"]`);

    cardRed.classList.toggle("red");

    --row;
    --column;
  }
}

function diagonalLeftDown(row,column)
{

  while(row<=size && column>=1)
  {
    let cardRed=document.querySelector(`[data-row="${row}"][data-col="${column}"]`);

    cardRed.classList.toggle("red");

    ++row;
    --column;
  }
}

function diagonalRightUp(row,column)
{

  while(row>=1 && column<=size)
  {
    let cardRed=document.querySelector(`[data-row="${row}"][data-col="${column}"]`);

    cardRed.classList.toggle("red");

    --row;
    ++column;
  }
}